# Install instructions here:
#   https://xgboost.readthedocs.io/en/latest/python/python_intro.html
from xgboost.sklearn import XGBClassifier
import xgboost as xgb
import matplotlib as plt
import numpy as np
from typing import Tuple, Dict, List, Callable
import pandas as pd


# def X_y (
#     df: pd.DataFrame,
#     data_fields: List[str],
#     label_of_interest: str,
#     ) -> Tuple[np.ndarray, np.ndarray]:
#     '''
#     Returns an n-dimensional array of features (X),
#     and 1-dimensional array of labels (y), using data from `eligible_census`.
#     '''
#     X = []
#     y = []
#     df2 = df.fillna(0)
#     for idx, row in df2.iterrows():
#         features = row[data_fields]
#         label = row[label_of_interest]
#         y.append(label)
#         X.append(features.values)
#     return np.array(X), np.array(y)
# def make_features (
#     df: pd.DataFrame
# ) -> np.array:
#     feats = []
#     df2 = df.fillna(0)
#     for i, row in df.iterrows():
#         feats.append(row)
#     return np.array(feats)

def align (
    features_df: pd.DataFrame,
    labels_df: pd.DataFrame,
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    '''
    Create two dataframes, where `geo_fips` align.
    '''
    # Only those labels for which we have features
    labels_df = labels_df[labels_df['geo_fips'].isin(features_df['geo_fips'])]
    # Sanity check that the geo_fips align!
    assert np.all(labels_df['geo_fips'] == features_df['geo_fips'])
    return features_df, labels_df


def X_y (
    features_df: pd.DataFrame,
    labels_df: pd.DataFrame,
    label: str,
    mapper: Callable = None,
) -> Tuple[np.array, np.array]:
    '''
    Returns two matrices, one of features (X)
    and one of labels (y).
    X should have a dimensionality SAMPLES x FEATURES,
    y should be 1-dimensional (SAMPLES x 1).
    '''
    # Filter out fields that are strings, or other non-numbers
    # We can only classify with floats and ints!
    features_df = features_df.select_dtypes(exclude=[object])
    # Align the two dfs on geo_fips,
    # excluding any labels for which we have no features
    f, l = align(features_df, labels_df)
    X = f.as_matrix()
    labels = l[label]
    if mapper:
        labels = labels.apply(mapper)
    y = labels.as_matrix()
    return X, y


def prior_probability (
    labels: np.array
) -> float:
    '''
    Prior probability of encountering a label `1`
    in `labels`.
    '''
    unique, counts = np.unique(labels, return_counts=True)
    count = dict(zip(unique, counts))[1]
    return count / len(labels)


def fresh_xgb (
    base_score=0.5
    ) -> XGBClassifier:
    return XGBClassifier(
        objective= 'binary:logistic',
        base_score=base_score,
        seed=27)


def xgb_cv (X,y,nfold=7,**kwargs):
    # eval_metrics:
    # http://xgboost.readthedocs.io/en/latest//parameter.html
    metrics = ['error@0.1', 'auc']
#     metrics = [ 'auc' ]
    # we use the @ syntax to override the default of 0.5 as the threshold for 0 / 1 classification
    # the intent here to to minimize FAR at the expense of FRR
    alg = fresh_xgb(**kwargs)
    xgtrain = xgb.DMatrix(X,y)
    param = alg.get_xgb_params()
    cvresults = xgb.cv(param,
                      xgtrain,
                      num_boost_round=alg.get_params()['n_estimators'],
                      nfold=nfold,
                      metrics=metrics,
                      early_stopping_rounds=100
                      )
    alg.set_params(n_estimators=cvresults.shape[0])
    alg.fit(X,y,eval_metric=metrics)
    return alg, cvresults


